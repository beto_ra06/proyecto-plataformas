# Proyecto Plataformas
Proyecto final del Curso de Programación Bajo Plataformas Abiertas ICiclo-2019
Integrantes: Astryd González Salazar, Alberto Rojas Arroyo, Joseline Sánchez Solís.

El proyecto consiste en realizar un juego que pone a prueba el nivel de conocimientos adquiridos en la carrera de Ingeniería Eléctrica, para lo cual se realizarán ciertas preguntas de cuatro cursos: Electromagnetismo I, Matemática Superior, Circuitos Lineales y Cicuitos Digitales.

Interacción del usuario con el programa:
    Al  inicio  se  despliega  un  menú  al  usuario,  donde  tendrá la opción de comenzar el juego, añadir pregunta o salirse.

Metodología y reglas del juego:
    Las preguntas son de selección múltiple; por lo tanto, cada pregunta tiene 3 respuestas posibles: a, b, c. El usuario debe escribir de forma correcta la opción que considera correcta. El juego consiste de 5 preguntas y si responde más de cuatro de forma correcta gana y se imprime en pantalla ”¡Felicidades, eres todo un ingeniero eléctrico!". Por el contrario, si no logra contestar más de 4 preguntas correctamente, se imprime en pantalla ”Necesitas estudiar”. Además, tendrá la opción de volver a jugar, ya sea que gane o pierda.
