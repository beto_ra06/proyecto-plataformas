/*
This file is part of ¿Quién quiere ser Ingeniero Eléctrico?.

¿Quién quiere ser Ingeniero Eléctrico? is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

¿Quién quiere ser Ingeniero Eléctrico? is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with ¿Quién quiere ser Ingeniero Eléctrico?.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "defines.h"
#include "respuestas.h"

question_t preguntas[1];
//variable que contiene la respuesta insertada por el usuario
char respuesta_usuario[50];
int puntaje = 0;
int turnos_jugados = 0;

//Función encargada de comparar la respuesta del usuario con la respuesta correcta
void comparador_respuestas(){
    printf("¿Cuál es la respuesta correcta?\n");
    scanf("\n%[^\n]", respuesta_usuario);

    //Si ya se contestaron 5 preguntas y respondió 4 de forma correcta, gana el juego
    if(turnos_jugados == 4 && puntaje >= 4){
        printf("\n* * * * * * * * * * * * * * * * * * * * * * * * *");
        printf("\nFIN DEL JUEGO\n");
        printf("¡Felicidades, eres todo un ingeniero eléctrico!\n");
        printf("* * * * * * * * * * * * * * * * * * * * * * * * *\n");
        exit(0);
    }

    //Si ya se contestaron 5 preguntas y respondió menos de 4 de forma correcta, pierde el juego
    else if(turnos_jugados == 4 && puntaje < 4){
        printf("\n* * * * * * * * * * * ");
        printf("\nFIN DEL JUEGO\n");
        printf("Necesitas estudiar\n");
        printf("* * * * * * * * * * * \n");
        exit(0);
    }
    //Si la respuesta del usuario es correcta
    else if(strcmp(preguntas->correct_answer, respuesta_usuario)==0){
        puntaje = puntaje + 1;
        turnos_jugados = turnos_jugados + 1;
        printf("\n* * * * * * * * * * ");
        printf("\n    Puntaje: %d\n", puntaje);
        printf("* * * * * * * * * * \n");
    }
    //Si responde de forma incorrecta
    else{
        printf("\n* * * * * * * * * * ");
        printf("\n    Puntaje: %d\n", puntaje);
        printf("* * * * * * * * * * \n");
        turnos_jugados = turnos_jugados + 1;
    }
}

//función que se encarga de imprimir la pregunta y sus opciones
void guardar_imprimir(int i, int random, char* buffer){
	memset(preguntas, 0, sizeof(question_t));

    char *p_pregunta_str = strtok(buffer,",");
	char *p_respuesta_str = strtok(NULL,",");
	char *p_opcion1_str = strtok(NULL,",");
	char *p_opcion2_str = strtok(NULL,",");
	char *p_opcion3_str = strtok(NULL,",\0");

	strcpy(preguntas->question, p_pregunta_str); //destino, lo que copio
	strcpy(preguntas->correct_answer, p_respuesta_str); //destino, lo que copio
	strcpy(preguntas->f_option, p_opcion1_str); //destino, lo que copio
	strcpy(preguntas->s_option, p_opcion2_str); //destino, lo que copio
	strcpy(preguntas->t_option, p_opcion3_str); //destino, lo que copio

	printf("%s \n a) %s \n b) %s \n c) %s \n", preguntas->question, preguntas->f_option, preguntas->s_option, preguntas->t_option);

    //Para pedir la respuesta al usuario y revisar si la respuesta está correcta
    comparador_respuestas();
}
