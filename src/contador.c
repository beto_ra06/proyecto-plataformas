/*
This file is part of ¿Quién quiere ser Ingeniero Eléctrico?.

¿Quién quiere ser Ingeniero Eléctrico? is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

¿Quién quiere ser Ingeniero Eléctrico? is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with ¿Quién quiere ser Ingeniero Eléctrico?.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdio.h>

// Función encargada de contar las lineas en un archivo de preguntas
int contador(char* path){

	FILE* archivo = fopen(path,"r");
	// Variable que guarda la cantidad de lineas que se cuenten
	int lineas = 0;
	//Se define una variable tipo char que contenga los caracteres de la 		linea
	char c;
	// La condición if verifica que el archivo exista
	if (archivo == NULL){
		return 0;
	}
	// Detecta el fin de una linea
	for (c = fgetc(archivo); c != EOF; c = fgetc(archivo)){
		if (c == '\n'){
			lineas = lineas + 1;
		}
	}
	fclose(archivo);
	// Retorna la cantidad de lineas contadas al finalizar el archivo
	return lineas;
}
