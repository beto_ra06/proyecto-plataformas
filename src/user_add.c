/*
This file is part of ¿Quién quiere ser Ingeniero Eléctrico?.

¿Quién quiere ser Ingeniero Eléctrico? is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

¿Quién quiere ser Ingeniero Eléctrico? is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with ¿Quién quiere ser Ingeniero Eléctrico?.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include "menu.h"
#include "defines.h"
#include "user_add.h"
#include <stdbool.h>
#include <string.h>
#include "preguntas_general.h"

/*
**Funcion: print_add_menu()

**Utilidad: Esta funcion imprime el menu que le indica al usuario de cuales
materias puede realizar las preguntas que desea agregar. También brinda la
opción de regresar al menú principal si lo desea.

**Argumentos: No recibe argumentos.

**Retorno: La función retorna ERROR o NO_ERROR dependiendo de si hubo un error o
no en el proceso de imprimir en pantalla.
*/
int print_add_menu(void){

    oper_t operaciones[oper_max - 1] = {
        {.opcion = oper_mate, .texto = "Matemática Superior"},
        {.opcion = oper_campo, .texto = "Electromagnetismo"},
        {.opcion = oper_digitales, .texto = "Circuitos Digitales"},
        {.opcion = oper_circuitos, .texto = "Circuitos Lineales"},
        {.opcion = oper_salir, .texto = "Volver al Menú Principal"}
    };

    printf("\n----------------------------------------\n");
    printf("           Materias disponibles\n");
    printf("----------------------------------------\n");

    for(int i = 0; i<oper_max - 1; i++){
        if(printf("\n%d. %s\n",operaciones[i].opcion,operaciones[i].texto) == 0){
            return ERROR;
        }
    }
}

/*
**Funcion: assign_correct_ans(unsigned int iter,question_t *question_array)

**Utilidad: Esta funcion brinda la posibilidad al usuario de escoger cuál de las
tres opciones que ingresó es la respuesta correcta, y la almacena en la posición
correspondiente a la pregunta en cuestión del arreglo de preguntas.

**Argumentos:
  -unsigned int iter: variable iteradora que indica la posición en el arreglo de
   tipo pregunta de la pregunta agregada por el usuario.
  -question_t *question_array: arreglo de tipo pregunta, donde se guarda cada
   pregunta que el usuario agregue, con sus respectivas opciones y respuesta
   correcta.

**Retorno: La función retorna ERROR o NO_ERROR dependiendo de si hubo un error o
no en el proceso de imprimir en pantalla.
*/
int assign_correct_ans(unsigned int iter,question_t *question_array){
    bool cont = false;
    unsigned int right_answer = 0;

        while(cont != true){
            if(printf("\nOpciones ingresadas:\n") == 0){
                return ERROR;
            }
            printf("\n1. %s\n",question_array[iter].f_option);
            printf("\n2. %s\n",question_array[iter].s_option);
            printf("\n3. %s\n",question_array[iter].t_option);

            user_selection(&right_answer,"¿Cuál de las opciones ingresadas es la correcta? ");

            if(right_answer == FIRST_OPTION){
                strcpy(question_array[iter].correct_answer,question_array[iter].f_option);
                clear_input();
                cont = true;
            }else if(right_answer == SECOND_OPTION){
                strcpy(question_array[iter].correct_answer,question_array[iter].s_option);
                clear_input();
                cont = true;
            }else if(right_answer == THIRD_OPTION){
                strcpy(question_array[iter].correct_answer,question_array[iter].t_option);
                clear_input();
                cont = true;
            }else{
                printf("\nPor favor ingrese una opción valida.\n");
                clear_input();
            }
        }

    return NO_ERROR;
}

/*
**Funcion: add_file(char *path, unsigned int number_questions)

**Utilidad: Esta funcion guarda los datos brindados por el usuario de la
pregunta que desea agregar, con sus respectivas opciones, y los almacena en un
arreglo de preguntas que posteriormente escribe en un archivo de tipo csv
(comma separated value) lo ingresado por el usuario en el formato que
corresponde.

**Argumentos:
  -char *path: ruta (PATH) del archivo correspondiente a la materia seleccionada
   por el usuario para agregar la pregunta.
  -unsigned int number_questions: numero de preguntas que el usuario quiere
   agregar.

**Retorno: La función retorna ERROR o NO_ERROR dependiendo de si hubo un error o
no en el proceso de imprimir en pantalla.
*/
int add_file(char *path, unsigned int number_questions){

    question_t question_array[number_questions];
    memset(question_array,0,sizeof(question_t)*number_questions);

    for(int i = 0;i<number_questions; i++){
        if(printf("\nIngrese la pregunta:\n") == 0){
            return ERROR;
        }

        /*
        Scanf deja un cambio de linea en el input, por lo tanto se necesita
        limpiarlo para poder leer lo que el usuario ingrese, utilizando
        fgets.
        */
        clear_input();
        if(fgets(question_array[i].question,MAX_Q,stdin)  == NULL){
            clear_input();
            return ERROR;
        }
        if(question_array[i].question[strlen(question_array[i].question) -1] == '\n'){
            question_array[i].question[strlen(question_array[i].question) -1] = '\0';
        }

        printf("\nIngrese las 3 opciones disponibles. Una de ellas debe ser la correcta:\n");

        if(printf("\nPrimera opción:\n") == 0){
            return ERROR;
        }
        if(fgets(question_array[i].f_option,MAX_A,stdin) == NULL){
            clear_input();
            return ERROR;
        }
        if(question_array[i].f_option[strlen(question_array[i].f_option) -1] == '\n'){
            question_array[i].f_option[strlen(question_array[i].f_option) -1] = '\0';
        }

        if(printf("\nSegunda opción:\n") == 0){
            return ERROR;
        }
        if(fgets(question_array[i].s_option,MAX_A,stdin) == NULL){
            clear_input();
            return ERROR;
        }
        if(question_array[i].s_option[strlen(question_array[i].s_option) -1] == '\n'){
            question_array[i].s_option[strlen(question_array[i].s_option) -1] = '\0';
        }

        if(printf("\nTercera opción:\n") == 0){
            return ERROR;
        }
        if(fgets(question_array[i].t_option,MAX_Q,stdin) == NULL){
            clear_input();
            return ERROR;
        }
        if(question_array[i].t_option[strlen(question_array[i].t_option) -1] == '\n'){
            question_array[i].t_option[strlen(question_array[i].t_option) -1] = '\0';
        }
        /*Se llama a la función que asigna la respuesta correcta.*/
        assign_correct_ans(i,question_array);
    }

    FILE *fp = fopen(path,"a");

    for(int i = 0; i<number_questions; i++){
        fprintf(fp,"%s,%s,%s,%s,%s,\n",question_array[i].question,
        question_array[i].correct_answer,question_array[i].f_option,
        question_array[i].s_option,question_array[i].t_option);
    }

    fclose(fp);
    return NO_ERROR;
}

/*
**Funcion: add_question(void)

**Utilidad: Esta funcion se encarga de tomar el dato de la cantidad de preguntas
que el usuario desea agregar, y detectar cual de las opciones desplegadas en el
menu escogió el usuario. Para ello también hace uso de otras funciones
declaradas anteriormente: print_add_menu(void), user_selection(unsigned int*
selection, char *text), add_file(char *path, unsigned int number_questions).

**Argumentos: No recibe argumentos.

**Retorno: La función retorna ERROR o NO_ERROR dependiendo de si hubo un error o
no en el proceso de imprimir en pantalla.
*/
int add_question(void){

    unsigned int num_questions = 0;
    unsigned int subject_option = 0;
    char more_questions;
    bool exit = false;
    bool exit_2 = false;

    while(exit != true){

        exit_2 = false;

        if(user_selection(&num_questions,"Ingrese la cantidad de preguntas que desea agregar: ") == ERROR){
            printf("Por favor ingrese una cantidad valida");
            continue;
        }
        clear_input();

        while(exit_2 != true){
            print_add_menu();
            if(user_selection(&subject_option,"Seleccione la materia en la que desea agregar la pregunta. Si desea salir ingrese 5: ") == ERROR){
                printf("Por favor ingrese una opción valida");
                clear_input();
                continue;
            }
            exit_2 = true;
        }

        switch(subject_option) {
            case oper_mate:
                add_file(PATH_MATE,num_questions);
                break;

            case oper_campo:
                add_file(PATH_ELECTRO,num_questions);
                break;

            case oper_digitales:
                add_file(PATH_DIGITALES,num_questions);
                break;

            case oper_circuitos:
                add_file(PATH_CIRCUITOS,num_questions);
                break;

            case oper_salir:
                exit = true;
                break;

            default:
                printf("\nOpción inválida, por favor intente de nuevo.\n");
                clear_input();
        }

        if(exit!=true){
            printf("\n¿Desea ingresar más preguntas? Escriba 'y' si su respuesta es Sí, 'n' si su respuesta es No: ");

            if(scanf("%s",&more_questions) != 1){
                clear_input();
                return ERROR;
            }
            if(strcmp(&more_questions,NO) == 0){
                exit = true;
            }
            clear_input();
        }
    }
    return NO_ERROR;
}
