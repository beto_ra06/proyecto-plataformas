/*
This file is part of ¿Quién quiere ser Ingeniero Eléctrico?.

¿Quién quiere ser Ingeniero Eléctrico? is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

¿Quién quiere ser Ingeniero Eléctrico? is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with ¿Quién quiere ser Ingeniero Eléctrico?.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdlib.h>
#include "preguntas_general.h"
#include "random_questions.h"
#include "defines.h"

/*
Posterior a la selección de la opción en el
menú principal "Jugar" se encarga de imprimir
un menú que contiene las categorías de las preguntas
*/


//Texto que se imprime en cada opción del menú
oper_t operaciones[oper_max - 1] = {
    {.opcion = oper_mate, .texto = "Matemática Superior"},
    {.opcion = oper_campo, .texto = "Electromagnetismo"},
    {.opcion = oper_digitales, .texto = "Circuitos Digitales"},
    {.opcion = oper_circuitos, .texto = "Circuitos Lineales"},
    {.opcion = oper_salir, .texto = "Volver al Menú Principal"}
};

//Función encargada de imprimir el menú y de llamar a las funciones requeridas para cumplir con la opción seleccionada
int preguntas_general(void){
    int salir = 1;
	do {
        //imprime el encabezado
        printf("\n----------------------------------------");
        printf("\nIndique la categoría que desea jugar:\n");
        printf("----------------------------------------\n");

        //imprime las opciones del menú
		for(int i = 0; i < oper_max - 1; i++){
		    printf("\n%d. %s\n", operaciones[i].opcion, operaciones[i].texto);
		}
		int entrada = 1;
        //lee la opción del usuario y lo guarda en la variable entrada.
		scanf("%d", &entrada);

		switch(entrada){
        //preguntas de mate superior
        case oper_mate: {
			//llama a la función random para la dirección PATH_MATE
		    random_quest(PATH_MATE);
		    break;
		}
        //preguntas de electromagnetismo
		case oper_campo: {
			//llama a la función random para la dirección PATH_ELECTRO
			random_quest(PATH_ELECTRO);
		    break;
		}
        //preguntas de digitales
        case oper_digitales: {
			//llama a la función random para la dirección PATH_DIGITALES
			random_quest(PATH_DIGITALES);
		    break;
		}
        // preguntas de circuitos lineales
        case oper_circuitos: {
			//llama a la función random para la dirección PATH_CIRCUITOS
			random_quest(PATH_CIRCUITOS);
		    break;
		}
        //para salir del menú de categorías e ir al menú principal
		case oper_salir:
		     salir = 0;
		     break;
        // si el usuario selecciona una opción inválida
		default:
		     printf("Operación inválida.\n");
		     exit(0);
		}
	 }while(salir);
}
