/*
This file is part of ¿Quién quiere ser Ingeniero Eléctrico?.

¿Quién quiere ser Ingeniero Eléctrico? is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

¿Quién quiere ser Ingeniero Eléctrico? is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with ¿Quién quiere ser Ingeniero Eléctrico?.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdbool.h>
#include "menu.h"
#include "defines.h"
#include "preguntas_general.h"
#include "user_add.h"

/*
**Funcion: print_add_menu(void)

**Utilidad: Esta funcion se encarga de imprimir en pantalla el menú principal
con las opciones del juego.

**Argumentos: No recibe argumentos.

**Retorno: La función retorna ERROR o NO_ERROR dependiendo de si hubo un error o
no en el proceso de imprimir en pantalla.
*/
int print_menu(void){
    /*
    Se define una estructura menu_t que contiene las opciones del menú y el
    texto correspondiente a cada opción.
    */
    typedef struct menu_s{
        enum main_menu_operations option;
        char *text;
    }menu_t;
    /*
    Se define un arreglo de tipo menu_t que contiene una opción con su
    correspondiente texto por cada elemento de este.
    */
    menu_t menu_array[] = {
        {.option = option_play, .text = "Jugar"},
        {.option = option_add, .text = "Agregar pregunta"},
        {.option = option_exit, .text = "Salir"}
    };

    for(int i = 0; i<option_last; i++){
        if(printf("\n%d. %s\n",menu_array[i].option + 1,menu_array[i].text) == 0){
            return ERROR;
        }
    }
}

/*
**Funcion: clear_input(void)

**Utilidad: Esta funcion limpia el imput para que no quede basura en caso de que
el usuario se equivoque o haya un cambio de linea indeseado.

**Argumentos: No recibe argumentos.

**Retorno: No retorna.
*/
void clear_input(void) {
    int c;
    while((c = getchar()) != '\n' && c != EOF);
}

/*
**Funcion: user_selection(unsigned int* selection, char *text)

**Utilidad: Esta funcion toma el dato ingresado por el usuario y la almacena en
una variable llamada selection.

**Argumentos:
  -unsigned int* selection: puntero a la variable selection de tipo entero.
  -char *text: texto que se despliega en pantalla antes de escanear lo que
   ingrese el usuario.

**Retorno: La función retorna ERROR o NO_ERROR dependiendo de si hubo un error o
no en el proceso de imprimir en pantalla.
*/
int user_selection(unsigned int* selection, char *text){

    if(printf("\n%s",text) == 0){
        return ERROR;
    }

    if(scanf("%d",selection) != 1){
        return ERROR;
        clear_input();
    }
    return NO_ERROR;
}

/*
**Funcion: menu_main(void)

**Utilidad: Esta funcion se encarga de detectar cual de las opciones desplegadas
en el menu escogió el usuario. Para ello hace uso de funciones declaradas
anteriormente: print_menu(void) y user_selection(unsigned int*selection, char
*text). Por cada posible opción a escoger, llama a otra función que realiza la
acción que el usuario desea.

**Argumentos: No recibe argumentos.

**Retorno: La función retorna ERROR o NO_ERROR dependiendo de si hubo un error o
no en el proceso de imprimir en pantalla.
*/
int menu_main(void){
    bool salir = false;
    while(salir != true){
        printf("\n----------------------------------------\n");
        printf("¿Quién quiere ser Ingeniero Eléctrico?\n");
        printf("----------------------------------------\n");
        print_menu();
        unsigned int selection = 0;

        if(user_selection(&selection,"Ingrese una opción: ") == ERROR){
            printf("\nPor favor ingrese una opcion valida\n");
            clear_input();
            continue;
        }

        switch(selection - 1) {
            case option_play:
            /*Llama a la función que se encarga de desplegar el menu con las
            tematicas disponibles para las preguntas e interactuar con el
            usuario*/
            preguntas_general();
            break;

            case option_add:
            /*Llama a la función que se encarga de desplegar el menu con las
            tematicas disponibles para las preguntas que el usuario desea
            añadir.*/
            add_question();
            break;

            case option_exit:
            /*Esta opción finaliza el programa.*/
            salir = true;
            break;

            default:
            printf("\nOpción inválida, por favor intente de nuevo.\n");
        }
    }
}
