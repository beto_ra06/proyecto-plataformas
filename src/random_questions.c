/*
This file is part of ¿Quién quiere ser Ingeniero Eléctrico?.

¿Quién quiere ser Ingeniero Eléctrico? is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

¿Quién quiere ser Ingeniero Eléctrico? is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with ¿Quién quiere ser Ingeniero Eléctrico?.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "contador.h"
#include "respuestas.h"
#include "random_questions.h"

//función que escoge un número de linea al azar
int random_quest(char *path){

	FILE* archivo = fopen(path, "r");
	//variable que contiene la cantidad de lineas
	int cant = contador(path);
	char buffer[BUFFER_LEN];
	//funciones que realizan la elección de un número random
	srandom (time(0));
	int rand = 0;
	//evita el caso en que rand sea igual a 0
	while (rand == 0){
		rand = random() % cant;
	}

    int contador = 1;

	//recorre las lineas del archivo en orden hasta llegar a la linea 			correspondiente a random
	while(fgets(buffer, BUFFER_LEN, archivo)||(contador <= rand)){

        if((contador == rand)||(rand == 0)){
			//función que se encarga de imprimir la linea correspondiente a 			random
			guardar_imprimir(contador, rand, buffer);
		}
		contador += 1;
		memset(buffer, 0, sizeof(char)*BUFFER_LEN);
	}

	fclose(archivo);
	return 0;
}
