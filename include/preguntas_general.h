/*
Se crea una enumeración correspondiente a las opciones del menu categorías.
*/
enum oper_e {
    oper_mate  = 1,
    oper_campo  = 2,
    oper_digitales = 3,
    oper_circuitos = 4,
    oper_salir = 5,
    oper_max
};

/*
Se define una estructura oper_t que contiene las opciones del menú principal y
el texto correspondiente a cada opción.
*/
typedef struct oper_s {
    enum oper_e opcion;
    char * texto;
}oper_t;

//Función encargada de imprimir la pregunta con tres posibles respuestas, según la categoría elegida
int preguntas_general();
