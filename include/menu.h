/*
Se crea una enumeración correspondiente a las opciones del menu principal
*/
enum main_menu_operations {
  option_play = 0,
  option_add,
  option_exit,
  option_last
};

int print_menu();
void clear_input(void);
int user_selection(unsigned int* selection,char *text);
int menu_main(void);
