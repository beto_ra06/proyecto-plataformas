#define ERROR -1
#define NO_ERROR 0
#define PATH_MATE "./files/matematica_superior.csv"
#define PATH_ELECTRO "./files/electromagnetismo.csv"
#define PATH_DIGITALES "./files/digitales.csv"
#define PATH_CIRCUITOS "./files/circuitos.csv"

#define MAX_Q 180 //Este tamaño debería ser suficiente para la pregunta.
#define MAX_A 100 //Este tamaño debería ser suficiente para cada respuesta.

/*
Se define una estructura question_t que contiene los elementos de la pregunta
que el usuario desea agregar: Pregunta(question), Respuesta
Correcta(correct_answer), Primer Opción(f_option), Segunda Opción(s_option),
Tercera Opción(t_option).
*/
typedef struct question_s {
    char question[MAX_Q];
    char correct_answer[MAX_A];
    char f_option[MAX_A];
    char s_option[MAX_A];
    char t_option[MAX_A];
}question_t;
