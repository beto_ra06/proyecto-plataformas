#define NO "n"
#define FIRST_OPTION 1
#define SECOND_OPTION 2
#define THIRD_OPTION 3

int print_add_menu(void);
int assign_correct_ans(unsigned int number_questions,question_t *question_array);
int add_file(char *path, unsigned int number_questions);
int add_question(void);
